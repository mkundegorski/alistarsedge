
#include <iostream>
#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/highgui.hpp>
#include <string>
#include <sstream>

using namespace cv;
using namespace std;



int main(int argc, char** argv){
	
	std::cout << "*****************************\n";
	std::cout << "     .::Alistars Edge::.\n";
	std::cout << "*****************************\n";
	std::cout << "Greetings! This program uses Canny edge detection algorithm to make your artifact drawing simpler and faster. To save current edge image press 's', to exit program press 'x' (If it doesn't work make sure the image window is the active one). \n";
	std::cout << "\n";
	std::cout << "*****************************\n";
	std::cout << "You can either load and image as a command line argument, or by default image named input.png (jpg/jpeg/bmp/tiff/gif) will be opened. This program lets you take different edge images in one run, numbering them, though it will overwrite them if you restart the program. So remember, back up! \n";
	std::cout << "\n";
	std::cout << "*****************************\n";

	std::cout << "This program is made my Mikolaj Kundegorski (name.surname@durham.ac.uk), Intelligent Imaging, Innovative Computing Group, Durham University, in 2014, on Creative Commons license. May you have a lot of fun! \n";
	std::cout << "\n";

	std::cout << "*****************************\n";
	std::cout << "\n";

	Mat img, imgDisplay;
	Mat greyImg;	
	Mat cannyImg;
	Mat imgIn;
			
	std::string iName;
	std::stringstream ss;
	namedWindow("img", CV_WINDOW_KEEPRATIO);
	namedWindow("edges", CV_WINDOW_KEEPRATIO);
	int blSize = 3;
	int e1 = 40;
	int e2 = 300;
	int e3 = 2;
	createTrackbar( "Gaussian blurr - bigger value less small detailes on the final img", "img", &blSize, 30);
	createTrackbar( "EdgeDet low thereshold", "img", &e1, 200);
	createTrackbar( "EdgeDet high threshold", "img", &e2, 600);
	createTrackbar( "EdgeDet kernel size (3,5,7)", "img", &e3, 2);
		
		
	if(argc == 2){//load image as is
		imgIn = imread(argv[1],-1);
		iName = argv[1];
	}
	else{
		iName = "input.png";
		imgIn = imread("input.png",-1);
		if(imgIn.empty())
			imgIn = imread("input.jpg",-1);
		if(imgIn.empty())
			imgIn = imread("input.tiff",-1);
		if(imgIn.empty())
			imgIn = imread("input.bmp",-1);
		if(imgIn.empty())
			imgIn = imread("input.jpeg",-1);
		if(imgIn.empty())
			imgIn = imread("input.gif",-1);

	}	
	if(!imgIn.empty()){
			std::cout << "Loaded " << iName << "\n";	
			//std::cout << "Image type is " << img.type() << "\n";
			bool keepProcessing = true;
			char c = '0';
			int saveIter = 0;

			while(keepProcessing){
				blur(imgIn,img,Size(2*blSize+1,2*blSize+1));
				cvtColor( img, greyImg, CV_BGR2GRAY );
				//resize(img, imgDisplay, Size(greyImg.rows/4,greyImg.cols/4), CV_INTER_LINEAR);
				imshow("img",imgIn);
				Canny(greyImg,cannyImg,e1,e2,2*e3+3);
				//resize(img, imgDisplay, Size(greyImg.rows/4,greyImg.cols/4), CV_INTER_LINEAR);
				imshow("edges",cannyImg);

				c = waitKey(40);
				if(c=='x'){
					keepProcessing = false;
					//c='s'; //always save an image on exit
				}
				if(c=='s'){
					
					vector<int> compression_params;
					compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
					compression_params.push_back(9);
					std::string sName;
					sName = "edges" + std::to_string(saveIter) + iName;
					imwrite(sName, cannyImg, compression_params);
					std::cout << "Saved edge image " << sName <<"\n";
					saveIter++;
				}
					
			}
			
			//waitKey(0);
			std::cout << "Take care! Bye!\n";
			return 0;

		}
		else{
			std::cout << "Image seems empty\n";
			return -1;
		}
	
	//std::cout << "Provide an image as a command line argument\n";
	return -1;	
}
