# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/miko/repositories/alistarsedge/source/alistarsEdges.cpp" "/home/miko/repositories/alistarsedge/build/CMakeFiles/alistarsEdges.dir/alistarsEdges.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/opt/opencv-3.1.0-dbg/include/opencv"
  "/opt/opencv-3.1.0-dbg/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
